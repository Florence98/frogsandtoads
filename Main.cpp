#include <iostream>
#include <cstring>
#include "FrogsAndToads.h"

using namespace std;

int main()
{
	cout << "**                                   **" << endl;
	cout << "**  Welcome to Frogs and Toads Game  **" << endl;
	cout << "**                                   **" << endl;
	cout << endl;

	cout << "Please enter the number of frogs" << endl;
	cout << "and toads that are playing today" << endl;
	cout << endl;

	int nFrogs = 0;
	cin >> nFrogs;
	cout << endl;

	cout << "Who will be solving the puzzle today?" << endl;
	cout << endl;
	cout << "Press H for Human or C for Computer" << endl;
	cout << endl;

	char whichGame;
	cin >> whichGame;
	cout << endl;

	if (whichGame == 'H' || whichGame == 'h')
	{
		HumanPlay(nFrogs);
	}
	else if (whichGame == 'C' || whichGame == 'c')
	{
		ComputerPlay(nFrogs);
	}
	else
	{
		cout << "I didn't recognise that command" << endl;
	}

	return 0;
}

void HumanPlay(int nFrogs)
{
	int nToads = nFrogs;

	int playStripSize = nFrogs + nToads + 2 + 1 + 1; // frogs, toads, two end slots, one empty slot and one null terminator.

	char* pGameState = new char[playStripSize];
	char* pTargetlGameState = new char[playStripSize];

	SetInitialGameState(pGameState, nFrogs, nToads);
	SetTargetGameState(pTargetlGameState, nFrogs, nToads);

	char userChoice;
	cout << endl;
	cout << "To play press F for frog move or T for toad move" << endl;
	cout << endl;

	cout << "    " << pGameState;
	cout << endl;

	bool bFrogMovesAvailable = true;
	bool bToadMovesAvailable = true;

	while (bFrogMovesAvailable == true || bToadMovesAvailable == true)
	{
		cout << "  ";
		cin >> userChoice;

		if (userChoice == 'F' || userChoice == 'f')
		{
			if (MakeFrogMove(pGameState, playStripSize) == false)
			{
				cout << "No frog move is possilbe" << endl << endl;
				bFrogMovesAvailable = false;
            }
		}
		else if (userChoice == 'T' || userChoice == 't')
		{
			if (MakeToadMove(pGameState, playStripSize) == false)
			{
				cout << "No toad move is possilbe" << endl << endl;
				bToadMovesAvailable = false;
			}
		}
		else
		{
			cout << "I didn't recognise that command" << endl;
		}

        cout << "    " << pGameState << endl;


		if (strcmp(pGameState, pTargetlGameState) == 0)
		{
			cout << endl;
			cout << "You did it! You have solved the puzzle" << endl;
			cout << endl;
			bFrogMovesAvailable = false;
			bToadMovesAvailable = false;
		}
	}

	if (strcmp(pGameState, pTargetlGameState) != 0)
	{
		cout << endl;
		cout << "You went down a dead end - never mind you're only human." << endl;
		cout << endl;
	}

	delete[]pGameState;
	delete[]pTargetlGameState;

	cout << "Press 'C' to see the computer solution or 'X' to finish the game" << endl;
    cout << endl;
	cin >> userChoice;

	if (userChoice == 'C' || userChoice == 'c')
		ComputerPlay(nFrogs);
}

void SetInitialGameState(char* pGameState, int nFrogs, int nToads)
{
	int index = 0;

	pGameState[index] = '-';
	index++;

	while (nFrogs != 0)
	{
		pGameState[index] = 'F';
		index++;
		nFrogs--;
	}

	pGameState[index] = '_';
	index++;

	while (nToads)
	{
		pGameState[index] = 'T';
		index++;
		nToads--;
	}

	pGameState[index] = '-';
	index++;

	// Null terminate
	pGameState[index] = '\0';
}

void SetTargetGameState(char* pGameState, int nFrogs, int nToads)
{
	int index = 0;

	pGameState[index] = '-';
	index++;

	while (nToads)
	{
		pGameState[index] = 'T';
		index++;
		nToads--;
	}

	pGameState[index] = '_';
	index++;

	while (nFrogs != 0)
	{
		pGameState[index] = 'F';
		index++;
		nFrogs--;
	}

	pGameState[index] = '-';
	index++;

	// Null terminate
	pGameState[index] = '\0';
}

bool MakeFrogMove(char* pGameState, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (pGameState[i] == 'F')
		{
			if (pGameState[i + 1] == '_')
			{
				// make slide move
				pGameState[i] = '_';
				pGameState[i + 1] = 'F';
				return true;
			}

			if (pGameState[i + 1] == 'T' &&
				pGameState[i + 2] == '_')
			{
				// make jump move
				pGameState[i] = '_';
				pGameState[i + 2] = 'F';
				return true;
			}
		}
	}
	return false;
}

bool MakeToadMove(char* pGameState, int size)
{
	for (int i = 0; i < size; i++)
	{
		if (pGameState[i] == 'T')
		{
			if (pGameState[i - 1] == '_')
			{
				pGameState[i] = '_';
				pGameState[i - 1] = 'T';
				return true;
			}

			if (pGameState[i - 1] == 'F' &&
				pGameState[i - 2] == '_')
			{
				pGameState[i] = '_';
				pGameState[i - 2] = 'T';
				return true;
			}
		}
	}
	return false;
}

void ComputerPlay(int nFrogs)
{
	int nToads = nFrogs;

	int playStripSize = nFrogs + nToads + 2 + 1 + 1; // frogs, toads, two end slots, one empty slot and one null terminator.

	char* pGameState = new char[playStripSize];
	SetInitialGameState(pGameState, nFrogs, nToads);

	GameStateNode* pRoot = new GameStateNode(pGameState, playStripSize, 'R');

	while (AddNewMoves(FindOpenNode(pRoot)) != false){}

	char* pTargetlGameState = new char[playStripSize];
	SetTargetGameState(pTargetlGameState, nFrogs, nToads);

	SearchTreeForGameState(pRoot, pTargetlGameState);

	delete (pRoot);

	char userChoice;
    cout << endl << "Press 'X' to finish the game" << endl;
    cout << endl;
	cin >> userChoice;
}

GameStateNode::GameStateNode(char* playstrip, int size, char move)
{
	m_pFrogBranch = NULL;
	m_pToadBranch = NULL;
	m_pParent = NULL;

	m_IsFrogBranchOpen = true;
	m_IsToadBranchOpen = true;

	m_playStripSize = size;
	m_Move = move;

	m_pGameState = new char[size];

	for (int i = 0; i < m_playStripSize; i++)
	{
		m_pGameState[i] = playstrip[i];
	}
}

GameStateNode::~GameStateNode()
{
	delete (m_pFrogBranch);
	delete (m_pToadBranch);
}


void GameStateNode::IsDeadEnd(GameStateNode* pBranchSendingMessage)
{
	if (m_pFrogBranch == pBranchSendingMessage)
		m_IsFrogBranchOpen = false;

	if (m_pToadBranch == pBranchSendingMessage)
		m_IsToadBranchOpen = false;

	if (m_IsToadBranchOpen == false &&
		m_IsFrogBranchOpen == false)
	{
		if (m_pParent)
			m_pParent->IsDeadEnd(this);
	}
}

GameStateNode* FindOpenNode(GameStateNode* node)
{
	if (node->m_pFrogBranch == NULL &&
		node->m_pToadBranch == NULL)
	{
		return node;
	}

	if (node->m_pFrogBranch &&
		(node->m_pFrogBranch->m_IsFrogBranchOpen ||
		 node->m_pFrogBranch->m_IsToadBranchOpen))
	{
		return FindOpenNode(node->m_pFrogBranch);
	}

	if (node->m_pToadBranch &&
		(node->m_pToadBranch->m_IsFrogBranchOpen ||
		 node->m_pToadBranch->m_IsToadBranchOpen))
	{
		return FindOpenNode(node->m_pToadBranch);
	}
	return NULL;
}

void SearchTreeForGameState(GameStateNode* node, char* gameStateToFind)
{
	if (strcmp(gameStateToFind, node->m_pGameState) == 0)
		DisplayPathToSolution(node);

	if (node->m_pFrogBranch != NULL)
		SearchTreeForGameState(node->m_pFrogBranch, gameStateToFind);

	if (node->m_pToadBranch != NULL)
		SearchTreeForGameState(node->m_pToadBranch, gameStateToFind);
}

void DisplayPathToSolution(GameStateNode* pSolutionNode)
{
	char moveType[260];
	char* gameState[260];

	int index = 0;

	while (pSolutionNode->m_pParent)
	{
		moveType[index] = pSolutionNode->m_Move;
		gameState[index] = pSolutionNode->m_pGameState;
		pSolutionNode = pSolutionNode->m_pParent;
		index++;
	}

	moveType[index] = pSolutionNode->m_Move;
	gameState[index] = pSolutionNode->m_pGameState;

	cout << endl;
	cout << "Computer Solution:" << endl << endl;

	while (index)
	{
		cout << "  " << moveType[index] << endl;
		cout << "    " << gameState[index] << endl;
		index--;
	}
	cout << "  " << moveType[index] << endl;
	cout << "    " << gameState[index] << endl;
}

bool AddNewMoves(GameStateNode* pBase)
{
	if (pBase == NULL)	// NULL means there are no more open branches in the tree - we have completed it!
		return false;

	// Make frog move
	GameStateNode* pPossFrogBranch = new GameStateNode(pBase->m_pGameState, pBase->m_playStripSize, 'F');

	if (MakeFrogMove(pPossFrogBranch->m_pGameState, pPossFrogBranch->m_playStripSize) == true)
	{
	    // Wire up the tree
		pPossFrogBranch->m_pParent = pBase;
		pBase->m_pFrogBranch = pPossFrogBranch;
	}
	else
	{
		pBase->m_pFrogBranch = NULL;
		pBase->m_IsFrogBranchOpen = false;
		delete pPossFrogBranch;
	}

	// Make toad move
	GameStateNode* pPossToadBranch = new GameStateNode(pBase->m_pGameState, pBase->m_playStripSize, 'T');

	if (MakeToadMove(pPossToadBranch->m_pGameState, pPossToadBranch->m_playStripSize) == true)
	{
		pPossToadBranch->m_pParent = pBase;
		pBase->m_pToadBranch = pPossToadBranch;
	}
	else
	{
		pBase->m_pToadBranch = NULL;
		pBase->m_IsToadBranchOpen = false;
		delete pPossToadBranch;
	}

	// If neither moves are possible
	if (pBase->m_IsToadBranchOpen == false &&
		pBase->m_IsFrogBranchOpen == false)
	{
		// Tell parent!
		if (pBase->m_pParent)
			pBase->m_pParent->IsDeadEnd(pBase);
	}
	return true;
}
